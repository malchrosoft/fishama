<!DOCTYPE html>
<!--
Copyright MalchroSoft - Aymeric MALCHROWICZ. All right reserved.
The source code that contains this comment is an intellectual property
of MalchroSoft [Aymeric MALCHROWICZ]. Use is subject to licence terms.
-->
<!-- Header -->
<?php
	// On démarre la session (ceci est indispensable dans toutes les pages de notre section membre)
	session_start();

	include_once './includes/globalFonc.php';
	include_once './includes/Queries.php';
	$q = new Queries();

	if (checkSession($_SESSION))
	{
		$user = $q->getUser($_SESSION['login'], md5($_SESSION['pwd']));
//		var_dump($user);
		if ($pageActive == 'BACK_OFFICE' && $userRole !== 1)
		{
			header("Location: ./index.php");
		}
	}
	else
	{
		header("Location: ./login.php");
	}

	$themeName = "cyborg"; //$user->theme;
	if (isset($_GET))
	{
		if (isset($_GET['theme']))
		{
			$themeName = strtolower($_GET['theme']);
			if (!($themeName == 'flatly' || $themeName == 'cyborg' || $themeName == 'paper'))
			{
				$themeName = "cyborg";
			}
		}
	}
?>

<html lang="fr">
    <head>
        <meta charset="UTF-8">
        
		<link rel="icon" type="image/png" href="./includes/img/favicon.png" />
        <link rel="shortcut icon" type="image/png" href="./includes/img/favicon.png" />
        
		<title>FishaMa | 
			<?php
				if ($titre != null)
				{
					echo $titre;
				}
				else
				{
					echo 'Partage de fichiers';
				}
			?>
        </title>
        
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <!--<link rel="stylesheet" href="./includes/css/bootstrap-<?php echo $themeName; ?>.min.css" media="screen">-->
        <!--<link rel="stylesheet/less" href="./includes/css/bootswatch-<?php echo $themeName; ?>.less" media="screen">-->
		<link href="./includes/css/material-kit.min.css?v=2.0.7" rel="stylesheet" />
        <link rel="stylesheet/less" href="./includes/css/fishama.less" media="screen">
		<link rel="stylesheet/less" href="./includes/css/left-menu-panel.less" media="screen">
        <link rel="stylesheet" href="./includes/css/font-awesome.min.css">

		<!--<script type="text/javascript" src="./includes/js/jquery.js"></script>-->
		<script type="text/javascript" src="./includes/js/core/jquery.min.js"></script>
		<script type="text/javascript" src="./includes/js/less-2.7.2.min.js" ></script>
		
        <meta content="FishaMa, fichiers, partage, Partage de fichiers, File Share Manager">

        <!--<script type="text/javascript" src="./includes/js/bootstrap.min.js"></script>-->
		
		<style>
			body {
				/*background-image: url(./includes/images/back_<?php echo rand(1, 14); ?>.jpg);*/
			}
		</style>
    </head>
    <body>
		<main>
			<nav class="navbar navbar-expand-lg bg-dark <?php echo $_SERVER['SERVER_NAME'] === "localhost" || 
					($_SERVER['SERVER_NAME'] !== "apps.malchrosoft.fr" && 
					$_SERVER['SERVER_NAME'] !== "apps.malchrosoft.com") ? "navbar-inverse" : "navbar-default"; ?> ">
				<div class="container-fluid">
					<a class="navbar-brand" href="./index.php" title="FishaMa - Gestion de partage de fichiers"><?php logo(); ?> <b>FishaMa</b></a>

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="navbar-toggler-icon"></span>
						<span class="navbar-toggler-icon"></span>
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarText">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link" href="javascript:;">Home <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="javascript:;">Features</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="javascript:;">Pricing</a>
							</li>
						</ul>
						<span class="form-inline">
							<span class="active-user float-right">
								<?php
									icon("user");
									echo "&nbsp; " . $user->nom;
									if ($user->role === 1)
									{
										echo "&nbsp;" . iconStr("certificate", "Connecté en tant qu'administrateur") . "&nbsp; ";
									}
									// On affiche un lien pour fermer notre session
									echo ' <a class="btn btn-default btn-xs" href="./logout.php">Déconnection</a>';
								?>
							</span>
						</span>
					</div>

				</div>
			</nav>
			<div class="site-container container-fluid">
				
				
				
				<!-- /Header -->

