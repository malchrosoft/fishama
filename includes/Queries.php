<?php

	class Queries
	{
		var $connexion;
		
		public function __construct($rootPath = ".")
		{
			if (strpos($_SERVER['SERVER_ADDR'],"192.168.") > -1 ||
				$_SERVER['SERVER_NAME'] == "localhost")
			{
				include_once $rootPath . '/includes/malchrosoftConnexion.php';
			}
			else
			{
				include_once $rootPath . '/includes/connexion.php';
			}
			include_once $rootPath . '/includes/classes/User.php';
			include_once $rootPath . '/includes/classes/File.php';
			
			
			$this->connexion = $connexion;
		}
		
		public function __destruct()
		{
			$this->connexion->close();
		}
		
		public function insertPrefix($tableName, $fieldsList)
		{
			return "INSERT INTO fishama_$tableName ($fieldsList) VALUES ";
		}

		public function executeInsert($sql)
		{
//        echo '<br/>EXECUTE : ';
//        var_dump($sql);
			if ($this->connexion->query($sql) === TRUE)
			{
				return $this->connexion->insert_id;
			}
			return -1;
		}

		public function delete($fromTable, $idFieldName, $idValues)
		{
			$sql = "DELETE from fishama_$fromTable where $idFieldName in ($idValues);";
			if ($this->connexion->query($sql) === TRUE)
			{
				return TRUE;
			}
			else
			{
				return "Error deleting record: " . $this->connexion->error;
			}
		}
		
		/**
		 *
		 * @param type $tableNameWithAliasList tables names from clause
		 * like 'table1 t1, table2 t2, table3 t3', spaces after ',' is important
		 * @param $selectClause '*' by default
		 */
		public function selectPrefix($tableNameWithAliasList, $selectClause = '*')
		{
			return "SELECT $selectClause FROM fishama_" . str_replace(", ", ", fishama_", $tableNameWithAliasList) . " ";
		}

		public function executeSelect($sql)
		{
//			echo '<br/>EXECUTE : ';
//			var_dump($sql);
			return $this->connexion->query($sql);
		}
		
		private function extractUser($getUserResult)
		{
			if ($getUserResult === FALSE)
			{
				return null;
			}
			$row = $getUserResult->fetch_array(MYSQLI_ASSOC);
			if ($row === FALSE)
			{
				return null;
			}
			$user = new User($row['id'], $row['nom'], $row['login'], $row['password_md5'], $row['email'], $row['role']);
			if ($user->id == null)
			{
				return null;
			}
//			var_dump($user);
			$getUserResult->close();
			return $user;
		}
		
		/**
		 * 
		 * @param int $id
		 * @return type
		 */
		public function getUserById(int $id)
		{
			$result = $this->executeSelect($this->selectPrefix('user u') . "WHERE id = $id;");
			return $this->extractUser($result);
		}
		
		/**
		 * 
		 * @param string $login
		 * @param string $pwd_md5 put null to get user without password check
		 * @return \User
		 */
		public function getUser(string $login, string $pwd_md5 = null)
		{
			$result = $this->executeSelect($this->selectPrefix('user u') . "WHERE login = LOWER('" . $login . "')"
				.($pwd_md5 != null ? " and password_md5 = '" . $pwd_md5 . "'" : "") 
				.";");
			return $this->extractUser($result);
		}
		
		/**
		 * 
		 * @param User $user
		 * @return boolean true if user was saved, false otherwise
		 */
		public function saveUser(User $user, $actionUserId = -1)
		{
			if ($user->id == null)
			{
				// create case
				$sql = $this->insertPrefix("user u", "id, nom, login, password_md5, email, role").
					"($user->id, $user->nom, $user->login, $user->password_md5, $user->email, $user->role);";
				return $this->executeInsert($sql);
			}
			
			$existingUserLogin = $this->getUser($user->login);
			if ($user->id != $existingUserLogin->id)
			{
				return "Identifiant déjà utilisé";
			}
			
			// update case
			$sql = "UPDATE fishama_user SET nom = '$user->nom', login = '$user->login', email = '$user->email', role = '$user->role', "
				."password_md5 = '$user->password_md5' WHERE id = $user->id;";
			return $this->executeSelect($sql);
		}
		
		/**
		 * @param type $userId
		 * @param type $actionUserId
		 * @return type
		 */
		public function deleteUser($userId, $actionUserId = -1)
		{
			return $this->delete("user", "id", "$userId");
		}
		
		public function getUserFiles($userId, $id_folder = -1)
		{
			$sql = $this->selectPrefix("file f") . "
				left join fishama_file_user ffu on (f.id = ffu.id_file)
				left join user u on (f.id_user = u.id)
				where f.id_user = $userId or ffu.id_user = $userId;";
			
			$result = $this->executeSelect($sql);
			$userFiles = array();
			if ($result === FALSE)
			{
				return $userFiles;
			}
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				var_dump($row);
				
				$user = new User($row['u.id'], $row['u.nom'], $row['u.login'], null, $row['u.email'], $row['u.role']);
				$file = new File($row['f.id'], $row['f.nom'], $row['u.description'], $row['file_type'], $row['file_path'], $row['last_update'], $user);
				$fu = new FileUser($user, $file, $row['ffu.rightCode']);
				
				$userFiles[] = $fu;
			}
			var_dump($user);
			$result->close();
			return $userFiles;
		}
		

	}

?>
