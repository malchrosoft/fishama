<?php
	
	/**
	 * Description of User
	 *
	 * @author amalchrowicz
	 */
	class User
	{
		public int $id;
		public string $nom;
		public string $login;
		public string $password_md5;
		public string $email;
		public int $role;
		
		function __construct(int $id, string $nom, string $login, string $password_md5, string $email, int $role)
		{
			$this->id = $id;
			$this->nom = utf8_encode($nom);
			$this->login = utf8_encode($login);
			$this->password_md5 = $password_md5;
			$this->email = $email;
			$this->role = $role;
		}
		
		function testPassword($pwd)
		{
			$pwd_md5 = md5($pwd);
			if ($pwd_md5 == $this->password_md5)
			{
				return true;
			}
			return false;
		}
		
		function setPassword(string $password)
		{
			$this->password_md5 = md5($password);
		}
	}
	