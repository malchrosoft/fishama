<?php

	/**
	 * Description of File
	 *
	 * @author amalchrowicz
	 */
	class File
	{
		public int $id;
		public string $nom;
		public string $description;
		public string $file_type;
		public string $file_path;
		public bool $isPrivate;
		public DateTime $last_update;
		public User $user;
		
		function __construct(int $id, string $nom, string $description, string $file_type, string $file_path, DateTime $last_update, User $user)
		{
			$this->id = $id;
			$this->nom = $nom;
			$this->description = utf8_encode($description);
			$this->file_type = $file_type;
			$this->file_path = $file_path;
			$this->last_update = $last_update;
			$this->user = $user;
		}
	}
	
	class FileUser
	{
		public User $user;
		public File $file;
		public string $rightCode;
		
		function __construct(User $user, File $file, string $rightCode)
		{
			$this->user = $user;
			$this->file = $file;
			$this->rightCode = $rightCode;
		}
		
		
	}
	