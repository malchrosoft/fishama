<?php
	$e = error_reporting(E_ALL);

	include_once './includes/Queries.php';
	$q = new Queries();

	if (isset($_POST['login']))
	{
		$login = str_replace(";", "_", $_POST['login']);
		$user = $q->getUser(strtolower($login), md5($_POST['pwd']));

		if ($user == null)
		{
			$isInvalid['login'] = true;
			$isInvalid['pwd'] = true;
		}

		if (!$isInvalid['login'] && !$isInvalid['pwd'])
		{
			// on vérifie les informations du formulaire, à savoir si le pseudo saisi est bien un pseudo autorisé, de même pour le mot de passe
			// dans ce cas, tout est ok, on peut démarrer notre session
			// on la démarre :)
			session_start();
			// on enregistre les paramètres de notre visiteur comme variables de session ($login et $pwd) (notez bien que l'on utilise pas le $ pour enregistrer ces variables)
			$_SESSION['login'] = strtolower($_POST['login']);
			$_SESSION['pwd'] = $_POST['pwd'];

			// on redirige notre visiteur vers une page de notre section membre
			header("location: ./index.php");
		}
	}
?>

<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>FishaMa - Login</title>

		<!-- Common  header -->
		<link href="./includes/css/font-awesome.min.css" rel="stylesheet" />
		<link href="./includes/css/material-kit.min.css?v=2.0.7" rel="stylesheet" />
		<link href="./includes/css/fishama.less" rel="stylesheet/less" />

		<script type="text/javascript" src="./includes/js/core/jquery.min.js"></script>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style>
			body {
				background-color: #0066cc;
				background-image: url(./includes/images/back_<?php echo rand(1, 14); ?>.jpg);
				background-position: center center;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-size: cover;
			}

			.login-dialog {
				width: 300px;
				height: 231px;
				margin-left: calc(-300px /2);
				margin-top: calc(-231px /2);

				border: none;
				/*border-radius: 10px;*/

				display: block !important;
				background-color: rgba(255, 255, 255, 0.5);
				box-shadow: 0 0 12px -4px rgba(22, 22, 22, 1);
				transition: box-shadow 2s ease-in-out 2s, background-color 4s ease-in-out 2s;
			}

			.login-dialog:hover {
				background-color: rgba(255, 255, 255, 0.8);
				box-shadow: 0 20px 12px -1px rgba(22, 22, 22, 0.6);
				transition-delay: 0s;
				transition-duration: 2s;
			}

			.login-dialog input:first-child {
				margin-top: 10px;
			}

			.login-dialog input {
				background-color: transparent;
				width: calc(100%);
			}

			input.has-error {
				background-color: rgba(175, 0, 0, 0.4);
			}

			.dialog-content .copyright {
				text-decoration: none;
				color: #0066cc;
				font-size: 0.9rem;
				text-align: center;
				text-shadow: 0 0 1px #222222;
				position: absolute;
				float: left;
				bottom: 0;
				left: 0;
				right: 0;
			}

			.dialog-content .copyright a {
				text-decoration: none;
				color: inherit;
			}

			::placeholder {
				color: #515151;
				opacity: 1;
			}

			.filig {
				/*transform: rotate(270deg);*/
				/*transform-origin: left bottom;*/
				position: fixed;
				bottom: 13px;
				right: 20px;
				color: white;
				opacity: 0.45;
				font-size: 5em;
				font-weight: 900;
				transition: all 700ms ease-in-out 500ms;
			}
			.filig:hover {
				opacity: 0.8;
				text-shadow: 0 0 5px #424242;
			}
		</style>

	</head>
	<body>
		<div class="dialog login-dialog">
			<div class="dialog-title">
				<i class="fa fa-lock"></i>&nbsp;&nbsp;Connexion utilisateur
				<!--<img src="./includes/images/jworg_red_base.svg" class="jw-logo-stable" />-->
			</div>
			<div class="dialog-content">
				<form action="#" method="post" class="form-horizontal">
					<input type="text" class="<?php echo $isInvalid['login'] ? "has-error" : ""; ?>"  name="login" placeholder="identifiant" />
					<input class="<?php echo $isInvalid['pwd'] || $isInvalid['login'] ? "has-error" : ""; ?>"  type="password" name="pwd" placeholder="mot de passe" />
					<div class="button-bar">
						<button type="submit" class="button"><i class="fa fa-check"></i></button>
						<button type="reset" class="button"><i class="fa fa-times"></i></button>
					</div>
				</form>
				<div class="copyright"><a target="_blank" href="https://fr.wikipedia.org/wiki/Copyright">Copyright</a> © <?php echo strftime("%Y"); ?>&nbsp;&nbsp;<i class="fa fa-share-alt-square"></i>&nbsp;FishaMa</div>
			</div>
		</div>

		<!-- Décoration -->
		<div class="oiseau-2">
			<img src="./includes/images/eagle_flying.gif" />
		</div>
		<div class="oiseau-3">
			<img src="./includes/images/eagle_flying_rtl.gif" />
		</div>

		<div class="filig"><i class="fa fa-share-alt-square">&nbsp;FishaMa</i></div>



		<!-- Common footer -->
		<script type="text/javascript" src="./includes/js/less-2.7.2.min.js" ></script>
		<script type="text/javascript" src="./includes/js/bootstrap.min.js" ></script>
	</body>
</html>


