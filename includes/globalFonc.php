<?php

	/**
	 * Constantes de calendriers
	 */
	# Aujourd'hui
	$aujourdhui = array
		(
		'j' => date('d'),
		'm' => date('m'),
		'a' => date('Y'),
		's' => date('W')
	);

	//---- Tableau des jours
	$conversionDate['jour'] = array
		(
		0 => 'Sem.',
		1 => 'Lundi',
		2 => 'Mardi',
		3 => 'Mercredi',
		4 => 'Jeudi',
		5 => 'Vendredi',
		6 => 'Samedi',
		7 => 'Dimanche'
	);
	//---- Tableau des mois
	$conversionDate['mois'] = array
		(
		'01' => 'janvier',
		'02' => 'février',
		'03' => 'mars',
		'04' => 'avril',
		'05' => 'mai',
		'06' => 'juin',
		'07' => 'juillet',
		'08' => 'août',
		'09' => 'septembre',
		'10' => 'octobre',
		'11' => 'novembre',
		'12' => 'décembre'
	);
	//---- Tableau des jour férié
	$conversionDate['off'] = array
		(
		101 => "Jour de l'an",
		105 => 'Fête du travail',
		805 => 'Victoire 1945',
		1407 => 'Fête nationale',
		1508 => 'Assomption',
		111 => 'Toussaint',
		1111 => 'Armistice 1918',
		2512 => 'Noël'
	);

	class FormMode
	{

		const CREATE = "CREATE";
		const UPDATE = "UPDATE";
		const DELETE = "DELETE";
		const PREPARE_DELETE = "PREPARE_DELETE";
		const PREPARE = "PREPARE";

	}

	/**
	 * Construit un lien externe
	 * @param type $link le chemin du lien
	 * @param type $label le text du lien
	 * @param type $title l'infobulle
	 * @param type $withBr true s'il doit passer à la ligne ensuite, false sinon
	 * @return rien mais fait un echo du lien
	 */
	function linkP($link, $label, $title = NULL, $withBr = false)
	{
		echo buildLinkStr($link, $label, $title, $withBr);
	}

	/**
	 * Construit un lien externe
	 * @param type $link le chemin du lien
	 * @param type $label le text du lien
	 * @param type $title l'infobulle
	 * @param type $withBr true s'il doit passer à la ligne ensuite, false sinon
	 * @return le lien construit
	 */
	function buildLinkStr($link, $label, $title = NULL, $withBr = false)
	{
		if ($title == null)
			$title = $label;
		$result = '<a target="_blank" href="' . $link . '" alt="' . $title . '" title="' . $title . '">' . $label . '</a>';
		if ($withBr)
			$result .= '<br/>';
		return $result;
	}

	/**
	 * Icon de partage dans un carré
	 */
	define("LOGO", "share-alt-square");

	function logo()
	{
		icon(LOGO);
	}

	function icon($name, $title = "", $size = 1)
	{
		echo iconStr($name, $title, $size);
	}

	function iconStr($name, $title = "", $size = 1)
	{
		return '<i title="' . $title . '"class="fa fa-' . $name . ' fa-' . $size . 'x"></i>';
	}

	function goToFormButtonStr($pageURL, $inputName, $inputValue, $btnTxt)
	{
		return '<form action="' . $pageURL . '" method="POST"><input type="hidden" name="' . $inputName . '" value="' .
			$inputValue . '"/><button type="submit" class="btn btn-primary btn-sm" title="Editer">' . $btnTxt . '</button></form>';
	}

	function getCopyrightPhrase($separator = " - ", $alignement = 'left')
	{
		return '<p style="text-align: ' . $alignement . ';">&copy; 2021' . (date("Y") > 2021 ? "-".date("Y") : "")  .
			'&nbsp;&nbsp;<b>' . iconStr(LOGO) . ' FishaMa</b>' . $separator .
			'Tous droits réservés' . $separator .
			buildLinkStr("http://fr.wikipedia.org/wiki/Propri%C3%A9t%C3%A9_intellectuelle", 'Propriété intellectuelle', "Définition détaillée sur Wikipedia", false) .
			'<br/></p>';
	}

	function checkSession($currentSession)
	{
		// On récupère nos variables de session
		if (isset($currentSession['login']) && isset($currentSession['pwd']))
		{
			// On teste pour voir si nos variables ont bien été enregistrées
			return true;
		}
		return false;
	}

	function temps_ecoule($date, $type)
	{
		$echo_annee = $echo_mois = $echo_jour = "";
		if ($type == "timestamp")
		{
			$date2 = $date; // depuis cette date
		}
		else if ($type == "date")
		{
			$date2 = strtotime($date); // depuis cette date
		}
		else
		{
			return "Non reconnu";
		}
		$date1 = date("U"); // à la date actuelle
		// ######## ANNEES ########
		if ((date('Y', $date1 - $date2) - 1970) > 0)
		{
			$echo_annee = (date('Y', $date1 - $date2) - 1970) . " an" . ((date('Y', $date1 - $date2) - 1970) > 1 ? 's' : '');
		}
		// ######## MOIS ########
		if ((date('m', $date1 - $date2) - 1) > 0)
		{
			$echo_mois = (date('m', $date1 - $date2) - 1) . " mois";
		}
		// ######## JOUR ########
		if ((date('d', $date1 - $date2) - 1) > 0)
		{
			$echo_jour = (date('d', $date1 - $date2) - 1) . " jour" . ((date('d', $date1 - $date2) - 1) > 1 ? 's' : '');
		}

		return '' . (!empty($echo_annee) ? $echo_annee . (!empty($echo_jour) ? ', ' : ' et ') : '') .
			(!empty($echo_mois) ? $echo_mois . (empty($echo_jour) ? '' : ' et ') : $echo_mois) .
			(!empty($echo_jour) ? $echo_jour : $echo_jour);
	}

	function temps_ecoule_full($date, $type)
	{
		if ($type == "timestamp")
		{
			$date2 = $date; // depuis cette date
		}
		elseif ($type == "date")
		{
			$date2 = strtotime($date); // depuis cette date
		}
		else
		{
			return "Non reconnu";
			exit();
		}
		$date1 = date("U"); // à la date actuelle
		$return = "";
		// ########  ANNEE ########
		if ((date('Y', $date1 - $date2) - 1970) > 0)
		{
			if ((date('Y', $date1 - $date2) - 1970) > 1)
			{
				$echo_annee = (date('Y', $date1 - $date2) - 1970) . " Anneés";
				$return = $echo_annee . ", ";
			}
			else
			{
				$echo_annee = (date('Y', $date1 - $date2) - 1970) . " Année";
				$return = $echo_annee . ", ";
			}
		}
		else
		{
			$echo_annee = "";
			$return = $return;
		}
		// ########  MOIS ########
		if ((date('m', $date1 - $date2) - 1) > 0)
		{
			$echo_mois = (date('m', $date1 - $date2) - 1) . " Mois ";
			if (!empty($echo_annee))
			{
				$return = $echo_annee . " et " . $echo_mois;
			}
			else
			{
				$return = $echo_mois;
			}
		}
		else
		{
			$echo_mois = "";
			$return = $return;
		}
		// ########  JOUR ########
		if ((date('d', $date1 - $date2) - 1) > 0)
		{
			if ((date('d', $date1 - $date2) - 1) > 1)
			{
				$echo_jour = (date('d', $date1 - $date2) - 1) . " Jours";
				if (!empty($echo_annee) OR ! empty($echo_mois))
				{
					$return = $return . $echo_mois . " et " . $echo_jour;
				}
				else
				{
					$return = $return . $echo_mois . $echo_jour;
				}
			}
			else
			{
				$echo_jour = (date('d', $date1 - $date2) - 1) . " Jour";
				if (!empty($echo_annee) OR ! empty($echo_mois))
				{
					$return = $return . $echo_mois . " et " . $echo_jour;
				}
				else
				{
					$return = $return . $echo_mois . $echo_jour;
				}
			}
		}
		else
		{
			$echo_jour = "";
			$return = $return;
		}
		// ########  HEURE ########
		if ((date('H', $date1 - $date2) - 1) > 0)
		{
			if ((date('H', $date1 - $date2) - 1) > 1)
			{
				$echo_heure = (date('H', $date1 - $date2) - 1) . " Heures";
				if (!empty($echo_annee) OR ! empty($echo_mois) OR ! empty($echo_jour))
				{
					$return = $echo_annee . $echo_mois . $echo_jour . " et " . $echo_heure;
				}
				else
				{
					$return = $echo_annee . $echo_mois . $echo_jour . $echo_heure;
				}
			}
			else
			{
				$echo_heure = (date('H', $date1 - $date2) - 1) . " Heure";
				if (!empty($echo_annee) OR ! empty($echo_mois) OR ! empty($echo_jour))
				{
					$return = $echo_annee . $echo_mois . $echo_jour . " et " . $echo_heure;
				}
				else
				{
					$return = $echo_annee . $echo_mois . $echo_jour . $echo_heure;
				}
			}
		}
		else
		{
			$echo_heure = "";
			$return = $return;
		}
		// ########  MINUTE ET SECONDE ########
		$virgule_annee = "";
		$virgule_mois = "";
		$virgule_jour = "";
		if (date('i', $date1 - $date2) > 0)
		{
			if (date('i', $date1 - $date2) > 1)
			{
				$echo_minute = round(date('i', $date1 - $date2)) . " Minutes";
				if (!empty($echo_annee) OR ! empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
				{
					if (!empty($echo_annee))
					{
						if (!empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_annee = ", ";
						}
					}
					if (!empty($echo_mois))
					{
						if (!empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_mois = ", ";
						}
					}
					if (!empty($echo_jour))
					{
						if (!empty($echo_heure))
						{
							$virgule_jour = ", ";
						}
					}
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . " et " . $echo_minute;
				}
				else
				{
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . $echo_minute;
				}
			}
			else
			{
				$echo_minute = round(date('i', $date1 - $date2)) . " Minute";
				if (!empty($echo_annee) OR ! empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
				{
					if (!empty($echo_annee))
					{
						if (!empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_annee = ", ";
						}
					}
					if (!empty($echo_mois))
					{
						if (!empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_mois = ", ";
						}
					}
					if (!empty($echo_jour))
					{
						if (!empty($echo_heure))
						{
							$virgule_jour = ", ";
						}
					}
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . " et " . $echo_minute;
				}
				else
				{
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . $echo_minute;
				}
			}
		}
		else
		{
			if (date('s', $date1 - $date2) > 1)
			{
				$echo_minute = round(date('s', $date1 - $date2)) . " Secondes";
				if (!empty($echo_annee) OR ! empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
				{
					if (!empty($echo_annee))
					{
						if (!empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_annee = ", ";
						}
					}
					if (!empty($echo_mois))
					{
						if (!empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_mois = ", ";
						}
					}
					if (!empty($echo_jour))
					{
						if (!empty($echo_heure))
						{
							$virgule_jour = ", ";
						}
					}
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . " et " . $echo_minute;
				}
				else
				{
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . $echo_minute;
				}
			}
			else
			{
				if (!empty($echo_annee) OR ! empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
				{
					if (!empty($echo_annee))
					{
						if (!empty($echo_mois) OR ! empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_annee = ", ";
						}
					}
					if (!empty($echo_mois))
					{
						if (!empty($echo_jour) OR ! empty($echo_heure))
						{
							$virgule_mois = ", ";
						}
					}
					if (!empty($echo_jour))
					{
						if (!empty($echo_heure))
						{
							$virgule_jour = ", ";
						}
					}
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . " et " . $echo_minute;
				}
				else
				{
					$return = $echo_annee . $virgule_annee . $echo_mois . $virgule_mois . $echo_jour . $virgule_jour . $echo_heure . $echo_minute;
				}
			}
		}
		return $return;
	}

	function minToHHmm($min)
	{
		if ($min < 0)
		{
			return;
		}
		$h = floor($min / 60);
		$m = $min % 60;
		return sprintf('%02d:%02d', $h, $m);
	}

?>
