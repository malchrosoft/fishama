<?php
	$e = error_reporting(E_ALL);
	$pageActive = "WELCOME";
	$titre = "Gestion de partage de fichiers";
	include_once './includes/header.php';

	$userRole = $user->role;
?>	
		<div class="left-menu-panel">
			<div class="left-menu">
				<div class="menu-toolbar top"></div>
				<div class="menu-container">
					<div class="menu-item" data-menu="tableauBord"><span><?php icon("home", "Tableau de bord", 2) ?>Tableau de bord</span></div>
					<div class="menu-item" data-menu="fichiers"><span><?php icon("file", "Gestion des fichiers", 2) ?>Fichiers</span></div>
					<div class="menu-item" data-menu="parametres"><span><?php icon("user", "Paramètres utilisateur", 2) ?>Paramètres</span></div>
					
					<?php 
						if ($userRole === 1)
						{
					?>	
					<div class="menu-separator"></div>
					<div class="menu-item" data-menu="administration"><span><?php icon("cog", "Administration", 2) ?>Administration</span></div>
					<?php
						
						}
					?>
					
				</div>
				<div class="menu-toolbar bottom"><span>《 </span></div>
			</div>
			<div id="rightContainer">
				<div id="uniqueActivePanel" class="active-panel"></div>

				
				<?php echo getCopyrightPhrase(); ?>

				<p class="text-muted small">
					<b><?php logo(); ?> FishaMa</b> for File Share Manager. <span class="sameLine">Created by Aymeric MALCHROWICZ &nbsp;<a href="mailto:aymeric.malchrowicz@malchrosoft.com" target="_blank"><?php icon("envelope", "Send email") ?></a></span><br/>
					<span class="sameLine">Based on <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.</span>
					<span class="sameLine">Theme based on <a href="https://www.creative-tim.com/product/material-kit" target="_blank">Material Kit</a>.</span>
					<span class="sameLine">Started the <span title="15 août 2021">15/08/2021</span>.</span>
					<br/>
					<span class="sameLine">Icons from <a href="http://fortawesome.github.io/Font-Awesome/" target="_blank">Font Awesome</a>.</span>
					<span class="sameLine">Web fonts from <a href="http://www.google.com/webfonts" target="_blank">Google</a>.</span>

					<br/>
					<?php echo $_SERVER['SERVER_ADDR'] . " / " . $_SERVER['SERVER_NAME']; ?>

				</p>

			</div>
		</div>

<script language="javascript">
	
	function loadPanel(panelName)
	{
		$.ajax({
			cache: false,
			url: './includes/' + panelName + '.php',
			type: 'POST',
			data: 'userId=' + <?php echo $user->id ?>,
			dataType: 'html',
			success: function (code_html, statut)
			{
				$("#uniqueActivePanel").html(code_html);	
				$("#uniqueActivePanel").children('[data-toggle="popover"]').popover();
			},
			error: function (resultat, statut, erreur)
			{
				if (erreur === "Not Found")
				{
//					alert("Panel inexistant : " + panelName + ".php");
					$("#rightContainer").children('.active-panel').html(
							"<h1>EN COURS DE DÉVELOPPEMENT</h1>\n<p>Panel inexistant : ./includes/" + 
							panelName + ".php</p>");
				}
			},
			complete: function (resultat, statut)
			{

			}
		});
	}
	
	
	menuItems = document.getElementsByClassName("menu-item");
	for (i = 0; i < menuItems.length; i++)
	{
		$(menuItems[i]).click(function () {
			panelName = $(this).data('menu')
			
			items = document.getElementsByClassName("menu-item");
			for (i = 0; i < items.length; i++)
			{
				$(items[i]).removeClass("selected")
			}	
			$(this).addClass("selected");
			loadPanel(panelName);
		});
	}
	
	$(menuItems[0]).addClass("selected");
	loadPanel($(menuItems[0]).data('menu'));
	
	
	
	
</script>


<?php
	include_once './includes/footer.php';
?>
