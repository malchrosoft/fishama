/**
 * Author:  amalchrowicz
 * Created: 16 août 2021
 */
DROP TABLE IF EXISTS fishama_group;
CREATE TABLE fishama_group (
	id				int(4)			auto_increment,	
	nom				varchar(120)	NOT NULL,
	CONSTRAINT pk_fishama_group PRIMARY KEY	(id)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

-- DEFAULT GROUPS (to protect of deletion)
INSERT INTO fishama_group (nom) VALUES 
('ADMINISTRATEUR'), ('UTILISATEUR');

DROP TABLE IF EXISTS fishama_user;
CREATE TABLE fishama_user (
	id				int			auto_increment,
	nom				varchar(100)	NOT NULL,
	login			varchar(40)		NOT NULL	UNIQUE,
	password_md5	char(32)		NOT NULL COMMENT 'Mot de passe chiffré par fonction md5() de php',
	email			varchar(255)	NOT NULL COMMENT 'E-mail obligatoire',
	role			tinyint		NOT NULL DEFAULT 2 COMMENT '1 = admin, 2 = user',
	CONSTRAINT pk_fishama_user PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

INSERT INTO fishama_user (id, nom, login, password_md5, email, role) VALUES
(-1, 'SYSTEM', 'FISHAMA_SYS', md5('!fishama_sys$007#'), 'invalid-email@malchrosoft.com', 1);

INSERT INTO fishama_user (nom, login, password_md5, email) VALUES
('Aymeric MALCHROWICZ', 'amalchrowicz', md5('aymeric211181'), 'aymericm@malchrosoft.com'),
('Normal User', 'nuser', md5('nuser'), 'n.user@malchrosoft.com');

DROP TABLE IF EXISTS fishama_user_group;
CREATE TABLE fishama_user_group (
	id_group		int(4)		NOT NULL DEFAULT 2,
	id_user			int(6)		NOT NULL,	
	last_update		datetime	NOT NULL DEFAULT now(),		
	id_user_update	int(6)		NOT NULL DEFAULT -1,
CONSTRAINT fk_fishama_user_group_group FOREIGN KEY (id_group) REFERENCES fishama_group(id),
CONSTRAINT fk_fishama_user_group_user FOREIGN KEY (id_user) REFERENCES fishama_user(id),
CONSTRAINT unique_fishama_user_group UNIQUE (id_group, id_user)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS fishama_folder;
CREATE TABLE fishama_folder (
	id			int(11)			auto_increment,
	nom			varchar(200)	NOT NULL,
	id_user			int(6)		NOT NULL,	
	id_parent		int(11)		,
	CONSTRAINT pk_fishama_folder PRIMARY KEY (id),
	CONSTRAINT fk_fishama_folder_user FOREIGN KEY (id_user) REFERENCES fishama_user(id),
	CONSTRAINT fk_fishama_folder_parent FOREIGN KEY (id_parent) REFERENCES fishama_folder(id),
	CONSTRAINT unique_fishama_folfer UNIQUE (nom, id_parent)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS fishama_folder_user;
CREATE TABLE fishama_folder_user (
	id_folder	int(11)		NOT NULL,
	id_user		int(6)		NOT NULL,
	right_code	varchar(1)	NOT NULL DEFAULT 'R' COMMENT 'R = read, E = edit, O = owner',
	CONSTRAINT fk_fishama_folder_user_user FOREIGN KEY (id_user) REFERENCES fishama_user(id),
	CONSTRAINT fk_fishama_folder_user_folder FOREIGN KEY (id_folder) REFERENCES fishama_folder(id),
	CONSTRAINT unique_fishama_folfer_user UNIQUE (id_folder, id_user)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS fishama_folder_group;
CREATE TABLE fishama_folder_group (
	id_folder	int(11)		NOT NULL,
	id_group	int(4)		NOT NULL DEFAULT 2,	
	right_code	varchar(1)	NOT NULL DEFAULT 'R' COMMENT 'R = read, E = edit, O = owner',
	CONSTRAINT fk_fishama_folder_group_folder FOREIGN KEY (id_folder) REFERENCES fishama_folder(id),
	CONSTRAINT fk_fishama_folder_group_group FOREIGN KEY (id_group) REFERENCES fishama_group(id),
	CONSTRAINT unique_fishama_folder_group	UNIQUE (id_folder, id_group)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS fishama_file;
CREATE TABLE fishama_file (
	id			int(11)			auto_increment,
	nom			varchar(200)	NOT NULL,
	description varchar(500)	NOT NULL,
	id_folder	int(11)		NOT NULL,
	id_user		int(6)		NOT NULL,
	file_type	varchar(7)	NOT NULL	DEFAULT 'UNKNOWN',
	file_path	text		NOT NULL,
	is_private	boolean		NOT NULL DEFAULT true,
	last_update	datetime	NOT NULL DEFAULT now() ON UPDATE now(),
	CONSTRAINT pk_fishama_file PRIMARY KEY (id),
	CONSTRAINT fk_fishama_file_user FOREIGN KEY (id_user) REFERENCES fishama_user(id),
	CONSTRAINT fk_fishama_file_folder FOREIGN KEY (id_folder) REFERENCES fishama_folder(id),
	CONSTRAINT unique_fishama_folfer_user UNIQUE (id_folder, nom)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS fishama_file_user;
CREATE TABLE fishama_file_user (
	id_file		int(11)		NOT NULL,
	id_user		int(6)		NOT NULL,
	right_code	varchar(1)	NOT NULL DEFAULT 'R' COMMENT 'R = read, E = edit, O = owner',
	CONSTRAINT fk_fishama_file_user_file FOREIGN KEY (id_file) REFERENCES fishama_file(id),
	CONSTRAINT fk_fishama_file_user_user FOREIGN KEY (id_user) REFERENCES fishama_user(id),
	CONSTRAINT unique_fishama_file_user UNIQUE (id_file, id_user)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

DROP TABLE IF EXISTS fishama_file_group;
CREATE TABLE fishama_file_group (
	id_file		int(11)		NOT NULL,
	id_group	int(4)		NOT NULL DEFAULT 2,	
	right_code	varchar(1)	NOT NULL DEFAULT 'R' COMMENT 'R = read, E = edit, O = owner',
	CONSTRAINT fk_fishama_file_group_file FOREIGN KEY (id_file) REFERENCES fishama_file(id),
	CONSTRAINT fk_fishama_file_group_group FOREIGN KEY (id_group) REFERENCES fishama_group(id),
	CONSTRAINT unique_fishama_file_group UNIQUE (id_file, id_group)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

-- Last 100 versions only max 9999 / file
DROP TABLE IF EXISTS fishama_file_update;
CREATE TABLE fishama_file_update (
	id_file		int(11)		NOT NULL,
	id_user		int(6)		NOT NULL,
	file_update	datetime	NOT NULL DEFAULT now(),
	file_path	text		NOT NULL,
	version		int(4)		NOT NULL,
	CONSTRAINT fk_fishama_file_update_file FOREIGN KEY (id_file) REFERENCES fishama_file(id),
	CONSTRAINT fk_fishama_file_update_user FOREIGN KEY (id_user) REFERENCES fishama_user(id),
	CONSTRAINT unique_fishama_file_update UNIQUE (id_file, version)
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;

-- Last 2 months actions only
DROP TABLE IF EXISTS fishama_stats;
CREATE TABLE fishama_stats (
	id				int(9)		auto_increment	PRIMARY KEY,
	user_action		text		NOT NULL,
	id_user			int(6)		NOT NULL,
	action_date		datetime	NOT NULL DEFAULT now()
)  ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;